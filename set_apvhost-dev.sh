#!/bin/bash

#@
#@ USAGE : sudo set_apvhost-dev.sh <domain>
#@ TASK  : Set apache virtual host on webserver for <domain>,
#@         create corresponding user and configure settings.
#@
#* by    : marcell dot ferenc dot uni at gmail dot com

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ ! -s $root_dir/adta.conf ]; then exit 2; fi

source $root_dir/adta.conf 

## test operating system
test_op_sys

## test if the script was launched as super user
is_root

## input
domain=$1

## set shell option for extended pattern matching
shopt -s extglob

## test input domain name
case $domain in
 @(www|git).*[!-.a-zA-Z0-9]*.@(com|hu|io)) echo "$errm invalid domain: <$domain>"; exit 2 ;;
 @(www|git).[a-zA-Z]*.@(com|hu|io)) user=${domain#*.} ;;
 *) echo "$errm invalid domain: <$domain>"; exit 2 ;;
esac

## information
echo "domain: $domain"
echo "user: $user"

## test /var/www/domain (domain without the www. prefix) 
if [ -d /var/www/$user ]; then
 echo "$errm domain already exist: /var/www/$user"; exit 2; fi

## test if user exists
case $(id -u $user &> /dev/null; echo $?) in
 0) echo "$errm user already exists: <$user>"; exit 2 ;;
 *) echo "$infm create new user: <$user>" ;;
esac

## add new, non root privileged user (domain)
adduser $user

## set user folder access permissions
user_rgx=${user//./\\.}; user_rgx=${user_rgx//-/\\-}
user_home=$( grep -Ew "$user_rgx" /etc/passwd | cut -d":" -f6 )
if [ -n "$user_home" ]; then
 chmod 0750 $user_home #0700
 chown -R $user:$user $user_home; fi

## create apache directory structure and permissions
doc_root=/var/www/$user/public
mkdir -p $doc_root
chown -R $user:$user $doc_root
chmod -R 755 /var/www

## apache configuration file
cnf=/etc/apache2/sites-available/$user.conf

## reference configuration file
#ref_cnf=/etc/apache2/sites-available/000-default.conf

set_avhost $user $doc_root > $conf

## enable virtual host file
a2ensite ${cnf##*/}

## restart apache service
service apache2 restart