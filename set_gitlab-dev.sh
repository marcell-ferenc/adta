#!/bin/bash

#@ USAGE :
#@
#@ TASK  : Install gitlab on Ubuntu 14.04
#@
#* by    : marcell dot ferenc dot uni at gmail dot com
#* from  : http://www.howtoforge.com/how-to-run-your-own-git-server-with-gitlabhq-on-ubuntu-14.04

apt-get update
apt-get install -y build-essential zlib1g-dev libyaml-dev libssl-dev libgdbm-dev libreadline-dev libncurses5-dev libffi-dev curl git-core openssh-server redis-server checkinstall libxml2-dev libxslt-dev libcurl4-openssl-dev libicu-dev

## install ruby 2.0 (or more)
mkdir /tmp/ruby && cd /tmp/ruby
wget http://ftp.ruby-lang.org/pub/ruby/2.1/ruby-2.1.2.tar.gz
tar zxvf ruby-2.1.2.tar.gz
cd ruby-2.1.2
./configure
make
make install

## install gem
gem install bundler --no-ri --no-rdoc

## create a user=git for GitLab
adduser --disabled-login --gecos 'GitLab' git

## install of GitLab shell
cd /home/git
sudo -u git -H git clone https://github.com/gitlabhq/gitlab-shell.git
cd gitlab-shell
sudo -u git -H git checkout v1.7.0
sudo -u git -H cp config.yml.example config.yml

## change config.yml
domain=marcellferenc.com
sed "s/.*gitlab_url:.*/gitlab_url: \"http:\/\/$domain\"/g" config.yml

## install GitLab shell
sudo -u git -H ./bin/install
