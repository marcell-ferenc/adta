#!/bin/bash

#@
#@ USAGE  : sudo setup_new_env.sh -t <options>
#@
#@ OPTIONS: -u - update installations (only version controlled programs)
#@          -h - help
#@
#@ TASK   : test and install different programs, all programs will be tested 
#@          and eventually installed that are in the list progs=( ... ).
#@          you can run this scrip multiple times without any problem.
#@
#@ NOTE   : this script demands for a <sudo> password at the beginning
#@          of the installation since it invokes the <apt-get install> program
#@          also download and install softwares into the </opt> directory.
#@          this script provides a <path.txt> file that contains path
#@          and functions that need to be added to the <.bashrc> file.
#@          the <.bashrc> file have to be verified and eventually corrected
#@          manually after run to be the following:
#@
#@                export PATH=$PATH:<path_1>:<path_2>:<path_n>
#@
#@          -the 64bit version of the programs are installed when it is possible
#@          -installation works fine on Ubuntu 14.04 LTS, 64bit
#@          -maybe the download links change by time, they need to be verified
#@           (cc2noncc, CRZ2RNX, teqc, Sublime Text 2)
#@
#@          build and installed through version control:
#@          -GMT5
#@          -RTKLIB
#@          -my ants GNSS scripts (later on)
#@
#* by     : marcell dot ferenc dot uni at gmail dot com

## CHANGE FOR YOUR NEEDS START -------------------------------------------------
##------------------------------------------------------------------------------
## programs to test and install
##------------------------------------------------------------------------------
progs=( ncftp svn git gfortran f77 g++ tkdiff xmgrace latex gmtmath cc2noncc CRZ2RNX rtkrcv subl csh tcsh ksh teqc ipython gimp inkscape parallel ) 
updates=( gmtmath rtkrcv ) 
## CHANGE FOR YOUR NEEDS END ---------------------------------------------------

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ ! -s $root_dir/adta.conf ]; then exit 2; fi

source $root_dir/adta.conf 

## test operating system
test_op_sys

## test if the script was launched as super user
is_root

## default variable
test=no
updt=no
out=$HOME/path.txt

##-------------------------------------------------------------------------------------------------
## rtklib
rtklib_loc=/opt
rtklib_bin=rtklib
rtklib_dev=rtklib-dev

## gmt5
gmt5_loc=/opt
gmt5_bin=gmt5
gmt5_dev=gmt5-dev
gmt5_dat=gmt5-data

## teqc
teqc_loc=/opt
teqc_bin=teqc 
teqc="teqc_Lx86_64s.tar.Z"
teqc_http="http://www.unavco.org/software/data-processing/teqc/development"
teqc_path="$teqc_http/$teqc"

## cc2noncc
cc2noncc_loc=/opt
cc2noncc="CC2NONCC_ESOCv6.5.tar.gz"
cc2noncc_unc="${cc2noncc%.tar.gz}"
cc2noncc_ftp="ftp://dgn6.esoc.esa.int/CC2NONCC"
cc2noncc_path="$cc2noncc_ftp/$cc2noncc"
p1c1bias_file="$cc2noncc_ftp/p1c1bias.hist"

## crz2rnx 
crz2rnx_loc=/opt
crz2rnx="RNXCMP_4.0.4_Linux_x86_64bit.tar.gz"
crz2rnx_unc="${crz2rnx%.tar.gz}"
crz2rnx_ftp="ftp://ftp.geodesia.ign.es/utilidades/compresores"
crz2rnx_path="$crz2rnx_ftp/$crz2rnx"

## parallel
parallel_loc=/opt
parallel_ftp="ftp://ftp.gnu.org/gnu/parallel"
##-------------------------------------------------------------------------------------------------

echo $tmp
echo $log

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## list of options the program will accept
optstring=uth

## interpret options
while getopts $optstring opt; do
 case $opt in
  u) updt=yes ;;
  t) test=yes ;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

case "$test" in no) echo "$errm missing option: -t"; exit 2 ;; esac

## answer during installation
echo Y > $tmp.ans

echo
##------------------------------------------------------------------------------
## install programs if they are NOT installed
##------------------------------------------------------------------------------
for prog in ${progs[@]}; do
 if [ $( type $prog &> /dev/null; echo $? ) -ne 0 ]; then
  txt=$( printf "%-15.15s - install\n" $prog | tee -a $log )
  case $prog in
    gmtmath) is_gmt=no ;;
   cc2noncc) is_cc2noncc=no ;;
    CRZ2RNX) is_crz2rnx=no ;;
    RTKNAVI) is_rtklib=no ;;
       subl) is_sublime=no ;;
       teqc) is_teqc=no ;;
   parallel) is_parallel=no ;;
        f77) echo $txt; apt-get install fort77 < $tmp.ans &>> $log ;;
        svn) echo $txt; apt-get install subversion < $tmp.ans &>> $log ;;
     tkdiff) echo $txt; apt-get install tkcvs < $tmp.ans &>> $log ;;
    xmgrace) echo $txt; apt-get install grace < $tmp.ans &>> $log ;;
      latex) echo $txt
             apt-get install texlive-latex-extra < $tmp.ans &>> $log ;;
    ipython) echo $txt
             apt-get install ipython < $tmp.ans &>> $log
             apt-get install ipython-notebook < $tmp.ans &>> $log ;;
          *) echo $txt; apt-get install $prog < $tmp.ans &>> $log ;;
  esac
 else printf "%-15.15s - exist\n" $prog | tee -a $log; fi
done

##------------------------------------------------------------------------------
## install the latest Generic Mapping Tools (GMT) if it is NOT installed
##------------------------------------------------------------------------------
if [ "$is_gmt" == "no" ]; then

 echo "GMT5            - install" | tee -a $log

 ## install easy_install and Sphinx for later GMT5 documentation
 if [ $( type easy_install &> /dev/null; echo $? ) -ne 0 ]; then
  echo "   -> install python-setuptools" | tee -a $log
  apt-get install python-setuptools < $tmp.ans &>> $log
  echo "   -> install Spihnx installing" | tee -a $log
  easy_install -U Sphinx &>> $log; fi

 ## install necessary programs and libraries
 if [ $( type cmake &> /dev/null; echo $? ) -ne 0 ]; then
  echo "   -> install additional programs (ghostscript, cmake, etc.)" | tee -a $log
  apt-get install ghostscript build-essential cmake libnetcdf-dev libgdal1-dev < $tmp.ans &>> $log; fi

 ## create directory for gshhg and dcw files
 mkdir -p $gmt5_loc/$gmt5_dat; cd $_

 ## gshhg files
 echo "   -> download GSHHG database" | tee -a $log
 ncftpget ftp://ftp.soest.hawaii.edu/gshhg/$( ncftpls -1 ftp://ftp.soest.hawaii.edu/gshhg/gshhg-gmt*.tar.gz | tail -1 ) &>> $log

 ## dcw files
 echo "   -> download DCW database" | tee -a $log
 ncftpget ftp://ftp.soest.hawaii.edu/dcw/$( ncftpls -1 ftp://ftp.soest.hawaii.edu/dcw/dcw-gmt*.tar.gz | tail -1 ) &>> $log

 ## uncompress gshhg and dcw files and remove archive
 echo "   -> uncompress GSHHG and DCW databases" | tee -a $log
 for item in gshhg*.tar.gz dcw-*.tar.gz; do
  tar -xvf $item; /bin/rm -f $item
  dirs+=( "${item%.tar.gz}" ); done &>> $log

 ## checkout the latest GMT5 version
 echo "   -> checkout GMT5" | tee -a $log
 cd $gmt5_loc
 svn checkout svn://gmtserver.soest.hawaii.edu/gmt5/trunk $gmt5_dev &>> $log

 ## cp cmake template
 echo "   -> create ConfigUser.cmake" | tee -a $log
 /bin/cp -f $gmt5_loc/$gmt5_dev/cmake/ConfigUserTemplate.cmake $gmt5_loc/$gmt5_dev/cmake/ConfigUser.cmake

 ## change 3 items in ConfigUser.cmake
 echo "   -> modify ConfigUser.cmake" | tee -a $log
 ## 1 - installation directory
 sed -i -e "s/.*CMAKE_INSTALL_PREFIX.*prefix_path.*/set (CMAKE_INSTALL_PREFIX \"${gmt5_loc////\\/}\/$gmt5_bin\")/g" $gmt5_loc/$gmt5_dev/cmake/ConfigUser.cmake
 ## 2 - path to gshhg files
 sed -i -e "s/.*GSHHG_ROOT.*/set (GSHHG_ROOT \"${gmt5_loc////\\/}\/$gmt5_dat\/${dirs[0]}\")/g" $gmt5_loc/$gmt5_dev/cmake/ConfigUser.cmake
 ## 3 - path to dcw files
 sed -i -e "s/.*DCW_ROOT.*/set (DCW_ROOT \"${gmt5_loc////\\/}\/$gmt5_dat\/${dirs[1]}\")/g" $gmt5_loc/$gmt5_dev/cmake/ConfigUser.cmake

 ## build and install GMT5
 echo "   -> build GMT5" | tee -a $log
 cd $gmt5_loc/$gmt5_dev
 mkdir build
 cd build
 { cmake ..; make; } &>> $log
 echo "   -> install GMT5" | tee -a $log
 make install &>> $log

 ## create GMT5 documentation
 echo "   -> build GMT5 documentation" | tee -a $log
 { make docs_man; make docs_html; make docs_pdf; make install; } &>> $log

 ## add GMT5/bin to .bashrc PATH
 if [ $( grep $gmt5_bin $HOME/.bashrc &> /dev/null; echo $? ) -ne 0 ]; then
  paths+=( $gmt5_loc/$gmt5_bin/bin ); fi
fi

##------------------------------------------------------------------------------
## update GMT5
##------------------------------------------------------------------------------
if [ "$updt" == "yes" ]; then

 echo "GMT5            - updating" | tee -a $log
 cd $gmt5_loc/$gmt5_dev

 ## update GMT5 source
 svn up &>> $log

 ## svn upgrade command was successful
 if [ $? -eq 0 ]; then

  ## exit_code: 0 - repository was up-to-date; other - repository was updated
  ec=$( tail -n 1 $log | grep "At revision" &> /dev/null; echo $? )
 
  ## repository was updated, so build and install 
  if [ $ec -ne 0 ]; then
   echo "   -> GMT5 repository was updated" | tee -a $log

   ## build and install GMT5
   echo "   -> build GMT5" | tee -a $log
   cd $gmt5_loc/$gmt5_dev/build
   { cmake ..; make; } &>> $log
   echo "   -> install GMT5" | tee -a $log
   make install &>> $log

  else echo "   -> GMT5 repository was up-to-date" | tee -a $log; fi
 else echo "   -> svn error (maybe network error)"; fi

fi

##------------------------------------------------------------------------------
## install cc2noncc software if it is NOT installed
##------------------------------------------------------------------------------
if [ "$is_cc2noncc" == "no" ]; then

 echo "cc2noncc        - installing" | tee -a $log

 cd $cc2noncc_loc
 ncftpget $cc2noncc_path &>> $log
 
 ## uncompress and remove archive
 tar -xvf $cc2noncc &>> $log; /bin/rm -f $_ &>> $log

 cd cc2noncc*
 gfortran -o cc2noncc *.f &>> $log
 ncftpget $p1c1bias_file &>> $log

 ## add cc2noncc to .bashrc PATH
 if [ $( grep cc2noncc $HOME/.bashrc &> /dev/null; echo $? ) -ne 0 ]; then
  paths+=( $( pwd ) ); fi
fi

##------------------------------------------------------------------------------
## install CRZ2RNX software if it is NOT installed
##------------------------------------------------------------------------------
if [ "$is_crz2rnx" == "no" ]; then

 echo "CRZ2RNX         - installing" | tee -a $log

 cd $crz2rnx_loc
 
 ## original source is not accessible via wget (ERROR 406)
 #wget http://terras.gsi.go.jp/ja/crx2rnx/RNXCMP_4.0.6_Linux_x86_64bit.tar.gz

 ncftpget $crz2rnx_path &>> $log

 ## uncompress and remove archive
 tar -xvf $crz2rnx &>> $log
 /bin/rm -f $_ &>> $log

 cd $crz2rnx_loc/$crz2rnx_unc/bin
 for item in *; do
  ln -s -f $item $( echo $item | tr '[:upper:]' '[:lower:]' ); done

 ## add CRZ2RNX to .bashrc PATH
 if [ $( grep -i crz2rnx $HOME/.bashrc &> /dev/null; echo $? ) -ne 0 ]; then
  paths+=( $crz2rnx_loc/$crz2rnx_unc/bin ); fi
fi

##------------------------------------------------------------------------------
## install RTKLIB
##------------------------------------------------------------------------------
if [ "$is_rtklib" == "no" ]; then
 apps=( pos2kml str2str rnx2rtkp convbin rtkrcv )
 echo "RTKLIB          - installing" | tee -a $log
 cd $rtklib_loc
 git clone https://github.com/tomojitakasu/RTKLIB.git $rtklib_dev &>> $log
 for app in ${apps[@]}; do
  echo "   -> $app install" | tee -a $log
  cd $rtklib_loc/$rtklib_dev/app/$app/gcc
  { make; make install; } &>> $log
 done
fi

##------------------------------------------------------------------------------
## update RTKLIB
##------------------------------------------------------------------------------
if [ "$updt" == "yes" ]; then
 apps=( pos2kml str2str rnx2rtkp convbin rtkrcv )
 echo "RTKLIB          - updating" | tee -a $log
 cd $rtklib_loc/$rtklib_dev

 ## update RTKLIB source
 git pull -u &>> $log

 ## git pull command was successful
 if [ $? -eq 0 ]; then

  ## exit_code: 0 - repository was up-to-date; other - repository was updated
  ec=$( tail -n 1 $log | grep "Already up-to-date" &> /dev/null; echo $? )
 
  ## repository was updated, so build and install 
  if [ $ec -ne 0 ]; then
   echo "   -> RTKLIB repository was updated" | tee -a $log
   for app in ${apps[@]}; do
    cd $rtklib_loc/$rtklib_dev/app/$app/gcc
    echo "   -> $app build" | tee -a $log
    make &>> $log
    echo "   -> $app install" | tee -a $log
    make install &>> $log
   done
  else echo "   -> RTKLIB repository was up-to-date" | tee -a $log; fi
 else echo "   -> git error (maybe network error)"; fi

fi

##------------------------------------------------------------------------------
## install teqc
##------------------------------------------------------------------------------
if [ "$is_teqc" == "no" ]; then

 echo "teqc            - installing" | tee -a $log

 cd $teqc_loc; mkdir -p $teqc_bin; cd $_

 wget $teqc_path &>> $log
 
 uncompress $teqc &>> $log; tar -xvf ${teqc%.Z} &>> $log

 ## add teqc to .bashrc PATH
 if [ $( grep $teqc_bin $HOME/.bashrc &> /dev/null; echo $? ) -ne 0 ]; then
  paths+=( $teqc_loc/$teqc_bin ); fi

fi

##------------------------------------------------------------------------------
## install Sublime Text 2
##------------------------------------------------------------------------------
if [ "$is_sublime" == "no" ]; then

 echo "Sublime Text 2  - installing" | tee -a $log

 ## sublime text 2
 ## add-apt-repository ppa:webupd8team/sublime-text-2
 ## apt-get update
 ## apt-get install sublime-text
 
 ## sublime text 3
 {
  add-apt-repository ppa:webupd8team/sublime-text-3
  apt-get update
  apt-get install sublime-text-installer
 } &>> $log

 ## download
 ## cd /opt
 ## wget -O sublime.tar.bz2 http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20x64.tar.bz2 &>> $log

 ## tar -xvf sublime.tar.bz2 &>> $log; /bin/rm -f $_ &>> $log
 ## /bin/mv -f Sublime\ Text\ 2 sublime_text_2

 ## ln -s /opt/sublime_text_2/sublime_text /usr/bin/sublime

fi

##------------------------------------------------------------------------------
## install parallel
##------------------------------------------------------------------------------
if [ "$is_parallel" == "no" ]; then

 {
  cd $parallel_loc
  ncftpget $parallel_ftp/$( ncftpls -1 $parallel_ftp/parallel-*.tar.bz2 | tail -1 ) &>> $log
  tar -xvf parallel-*.tar.bz2
  /bin/rm -rf parallel-*.tar.bz2
  parallel_dirs=( parallel-???????? )
  cd ${parallel_dirs[-1]}
  ./configure --prefix=$parallel_loc/parallel && make && make install
 } &>> $log

fi

##------------------------------------------------------------------------------
## add path of installed programs to .bashrc's PATH variable
##------------------------------------------------------------------------------
if [ ${#paths[@]} -ne 0 ]; then

 echo; echo "paths into the <.bashrc> file in <$HOME/path.txt>" | tee -a $log

 ## extend PATH if necessary
 printf "%s:\ \n" "export PATH=\$PATH" "${paths[@]}" | sed '$s/...$//' > $out; fi

## information
printf "%s\n" "look the files:" " $log" " $out"
