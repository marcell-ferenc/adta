#!/bin/bash

#@
#@ USAGE : sudo set_ufa-dev.sh <user>
#@ TASK  : Add <user> account and set user folder access permissions.
#@         If the user exists then set proper permissions.
#@
#* by    : marcell dot ferenc dot uni at gmail dot com

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if [ ! -s $root_dir/adta.conf ]; then exit 2; fi

source $root_dir/adta.conf 

## test operating system
test_op_sys

## test if the script was launched as super user
is_root

## backup adduser.conf file
cnf=/etc/adduser.conf
if [ ! -s $cnf.bcp ]; then cp $cnf $cnf.bcp; fi

## modify adduser.conf file for default directory permission
case $(grep "^DIR_MODE=0755" $cnf &> /dev/null; echo $?) in
 0) sed -i 's/^DIR_MODE=0755/DIR_MODE=0750/' $cnf ;;
 *) ;;
esac

## input
user=$1

## test input user name
case $user in
 *[!-.a-zA-Z0-9]*) echo "$errm invalid user: <$user>"; exit 2 ;;
 [a-ZA-Z]*) ;;
 *) echo "$errm invalid user: <$user>"; exit 2 ;;
esac

## test if user exists
case $(id -u $user &> /dev/null; echo $?) in
 0) echo "$infm user already exists: <$user>"; crt=no ;;
 *) echo "$infm create new user: <$user>"; crt=yes ;;
esac

## add new, non root privileged user if not exists
case $crt in yes) adduser $user ;; esac

## set user folder access permissions
user_rgx=${user//./\\.}; user_rgx=${user_rgx//-/\\-}
user_home=$( grep -Ew "$user_rgx" /etc/passwd | cut -d":" -f6 )
if [ -n "$user_home" ]; then
 chmod 0750 $user_home #0700
 chown -R $user:$user $user_home; fi